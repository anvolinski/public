# -*- coding: utf-8 -*-
"""
@author: a.volinsky
a.n.volinski@ya.ru
"""
#Заменять данную константу для проверки тестовых решений
TEST_SUDOKU = [[1,5,4,8,7,3,2,9,6],
               [3,8,6,5,9,2,7,1,4],
               [7,2,9,6,4,1,8,3,5],
               [8,6,3,7,2,5,1,4,9],
               [9,7,5,3,1,4,6,2,8],
               [4,1,2,9,6,8,3,5,7],
               [6,3,1,4,5,7,9,8,2],
               [5,9,8,2,3,6,4,7,1],
               [2,4,7,1,8,9,5,6,3]]


RIGHT_SOLUTION = 'корректно'
WRONG_SOLUTION = 'некорректно'

def check_filled(sudoku_list):
    if len(sudoku_list) != 9:
        print(WRONG_SOLUTION)
        return False
    for i in sudoku_list:
        if (len(i)) != 9:
            print(WRONG_SOLUTION)
            return False
    return True

def check_horizontal_line(sudoku_list, y):
    test_list = [1,2,3,4,5,6,7,8,9]
    for i in sudoku_list[y]:
        try:
            test_list.remove(i)
        except Exception:
            print(WRONG_SOLUTION)
            return False
    if len(test_list) > 0:
        print(WRONG_SOLUTION)
        return False
    return True

def check_vertical_line(sudoku_list, x):
    test_list = [1,2,3,4,5,6,7,8,9]
    for i in range(len(sudoku_list)):
        try:
            test_list.remove(sudoku_list[i][x])
        except Exception:
            print(WRONG_SOLUTION)
            return False
    if len(test_list) > 0:
        print(WRONG_SOLUTION)
        return False
    return True

def check_square(sudoku_list, x, y):
    test_list = [1,2,3,4,5,6,7,8,9]
    for i in range(3):
        for r in range(3):
            try:
                test_list.remove(sudoku_list[x+i][y+r])
            except Exception:
                print(WRONG_SOLUTION)
                return False
    if len(test_list) > 0:
        print(WRONG_SOLUTION)
        return False
    return True

def check_all(sudoku_list):
    if not check_filled(sudoku_list):
        return
    for i in range(len(sudoku_list)):
        if not check_horizontal_line(sudoku_list, i):
            return
        if not check_vertical_line(sudoku_list, i):
            return
    for i in range (0,7,3):
        for r in range (0,7,3):
            if not check_square(sudoku_list, i,r):
                return
    print (RIGHT_SOLUTION)
    

check_all(TEST_SUDOKU)
